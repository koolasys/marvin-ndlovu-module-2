///Program by Marvin Ndlovu - for MTN Business App Academy
///Program  to store all the winning apps of the MTN Business App of the Year Awards since 2012

//Main method
void main() {
  //Array of the winning apps and their years
  List<Map<String, dynamic>> winningApps = [
    {"name": "FNB", "year": 2012},
    {"name": "SnapScan", "year": 2013},
    {"name": "LIVE Inspect", "year": 2014},
    {"name": "WumDrop", "year": 2015},
    {"name": "Domestly", "year": 2016},
    {"name": "Shyft", "year": 2017},
    {"name": "Khula Ecosystem", "year": 2018},
    {"name": "Naked Insurance", "year": 2019},
    {"name": "EasyEquities", "year": 2020},
    {"name": "Ambani", "year": 2021}
  ];

  //The names of the apps before  sorting
  print("----  Winning Apps before sorting  -----");
  winningApps.forEach((i) => print(i["name"]));
  print(
      "--------------------------------------------------------------------------");

  //Sorting the array ascendingly on their names
  winningApps.sort((a, b) => a["name"].compareTo(b["name"]));
  print("\n\n");

  //The array of the apps after  sorting using their names
  print("----  Winning Apps after sorting  -----");
  winningApps.forEach((i) => print(i["name"]));
  print(
      "--------------------------------------------------------------------------");

  print("\n\n");

  //Printing the apps that won in 2017 and in 2018
  winningApps.forEach((app) {
    if (app["year"] == 2017 || app["year"] == 2018)
      print('${app["name"]} won the award in ${app["year"]}');
  });

  print("\n\n");
  print(
      "--------------------------------------------------------------------------");
  print("The winningApps array has ${winningApps.length} apps");
  print(
      "--------------------------------------------------------------------------");
