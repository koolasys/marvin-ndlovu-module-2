///Program by Marvin Ndlovu - for MTN Business App Academy
///Program to print info about the app

///Main method
void main() {
  App app = new App(
      "Matric Live", "Best Breakthrough Solution", "Lesego Finger", 2020);
  print("${app.toString()}");
}

///App class
class App {
  String _nm;
  String _ct;
  String _dv;
  int _yr;

  //App();
  App(this._nm, this._ct, this._dv, this._yr);

  ///App name getter method
  String get name {
    //print("App name is: $_nm");
    //Returns the name is all caps
    return _nm.toUpperCase();
  }

  ///App name setter method
  void set name(String name) => this._nm = name;

  ///App category getter method
  String get category {
    //print("$name is in the $_ct sector");
    return _ct;
  }

  ///App category setter method
  void set category(String cat) => this._ct = cat;

  ///App developer getter method
  String get developer {
    //print("$name was developed by $_dv");
    return _dv;
  }

  ///App developer setter method
  void set developer(String dev) => this._dv = dev;

  ///App category getter method
  int get year {
    //print('$name won the MTN Business App of the Year in $_yr');
    return _yr;
  }

  String toString() =>
      "The $name app, developed by $developer, won the $category category in the year $year.";

  ///App's winning year setter method
  void set year(int year) => this._yr = year;
}
