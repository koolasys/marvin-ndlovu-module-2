///Program by Marvin Ndlovu - for MTN Business App Academy
///Program to print the deveoper's details
import 'dart:io';

//Main program
void main() {
  printMyData();
}

///Method to print the author's data
void printMyData() {
  String name = "Marvin Ndlovu";
  String favApp = "EasyEquities";
  String myCity = "Johannesburg";

  print("My name is: $name \nMy favorite app is: $favApp \n$myCity is my city");
}
